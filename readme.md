# Starter Guide

## Node and NPM require version
- NodeJS LTS or NodeJS 6.x
- Npm LTS or NPM latest

## Installation
```shell
git clone https://gitlab.com/antarka/nodewiki.git
npm install
```

## Start
```shell
npm start
```

# Todo GA

- [x] Support multi-projet (multiple documentation).
- Amélioration du design.
- Hightlight du code (au minimum javascript,html,css)
- Amélioration du generator pour un arbre plus cohérent!
- Mettre en place un routing pour retrouver simplement tout les fichiers.
- [x] refonte de l'updateDoc.
