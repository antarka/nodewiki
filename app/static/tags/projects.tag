<projects>

    <h1>Select the project</h1>
    <ul>
        <li each={ projectName in opts.projects }>{ projectName }</li>
    </ul>

    <style scoped>
        h1 {
            font-size: 22px;
        }
        ul {
            display: flex;
            flex-direction: column;
            margin-top: 20px;
        }
        ul > li {
            height: 35px;
            display: flex;
            align-items: center;
            padding: 0 10px;
            margin-left: 0;
            border-radius: 1px;
        }
            ul > li:hover {
                background: #06F;
                color: #FFF;
                cursor: pointer;
            }
        ul > li + li {
            margin-top: 10px;
        }
    </style>

</projects>
