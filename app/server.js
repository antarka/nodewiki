"use stricts";

console.time("startTime");

// Require packages
const path          = require('path');
const fs            = require('fs');
const async         = require('async');
const koa           = require('koa');
const bodyParser    = require('koa-bodyparser');
const resJSON       = require('koa-json');
const router        = require('koa-router');
const serve         = require('koa-static');
const jade          = require('koa-jade');
const sessions      = require('koa-generic-session');
const md            = require("node-markdown").Markdown;

// Read "Docs directory and sort a JSON data structure!"
const doc_directory = path.join( __dirname , "../docs" );

function fRecursive( argPath , root ) {
    let localJSON = {};
    return new Promise( (Resolve,Reject) => {
        fs.readdir( argPath , (err,files) => {
            if(err) {
                reject(`Failed to read the following path directory => ${argPath}`);
            }
            async.eachSeries(files,(name,next) => {
                const path_to_file = argPath + "/" + name;
                fs.stat(path_to_file,(err,stats) => {
                    if(err) console.err(err);
                    const isDirectory = stats.isDirectory();
                    if(isDirectory) {
                        if(!root) {
                            localJSON[name] = {};
                            fRecursive(path_to_file,localJSON[name]).then( (res) => {
                                next(null);
                            });
                        }
                        else {
                            root[name] = {};
                            fRecursive(path_to_file,root[name]).then( (res) => {
                                next(null);
                            });
                        }
                    }
                    else {
                        if(!root) {
                            localJSON[name] = md(fs.readFileSync(path_to_file,'utf8'));
                            next(null);
                        }
                        else {
                            root[name] = md(fs.readFileSync(path_to_file,'utf8'));
                            next(null);
                        }
                    }
                });
            },() => {
                Resolve(localJSON);
            });
        });
    });
}

function requireDocumentation() {
    console.time("getDocumentation");
    let docs = {};
    return new Promise( (resolve,reject) => {
        fs.access(doc_directory, fs.R_OK | fs.W_OK, (err) => {
            if(err) throw new Error(err);

            async.waterfall([
                (callback) => {
                    fs.readdir( doc_directory , (err,files) => {
                        if(err) throw new Error(err);
                        callback(null,files);
                    });
                },
                (projectsDirectory,finalize) => {
                    let callbackStore = [];

                    async.each( projectsDirectory , (projectName,next) => {
                        const projectCallback = (end) => {
                            fRecursive( doc_directory + "/" + projectName ).then( (projectJSON) => {
                                docs[projectName] = projectJSON;
                                end(null, {} );
                            }).catch( (e) => {
                                console.err(e);
                            });
                        }
                        callbackStore.push(projectCallback);
                        next(null);
                    }, (err) => {
                        if(err) throw new Error(err);
                        async.parallel(callbackStore,(err,result) => {
                            finalize(null,result);
                        });
                    });
                }
            ], (err,result) => {
                if(err) throw new Error(err);
                console.timeEnd("getDocumentation");
                resolve(docs);
            });

        });
    });
}

requireDocumentation().then( (documentation) => {

    console.log("Starting http server..");

    console.log("Loading wiki configuration");
    const configuration = require("../configuration.json");

    console.time("httpStart");
    const app = koa();
    app.use( resJSON() );
    app.use( bodyParser() );
    app.use( serve( path.join(__dirname,'static') ) );
    app.use( sessions() );

    new jade({
        viewPath: path.join(__dirname,'views'),
        noCache: true,
        pretty: true,
        compileDebug: false,
        app: app
    });

    // Global using scope! (top middleware)
    app.use( function *(next) {
        const projetsName = [];
        for(const K in documentation) {
            projetsName.push(K);
        }
        this.projects = projetsName;
        this.doc = documentation;
        yield next;
    });

    // Auto update when file updated.
    fs.watch(doc_directory, {encoding: 'buffer'}, (event,filename) => {
        console.log("update documentation!");
        requireDocumentation().then( (newDocumentation) => {
            app.use( function *(next) {
                const projetsName = [];
                for(const K in documentation) {
                    projetsName.push(K);
                }
                this.projects = projetsName;
                this.doc = newDocumentation;
                yield next;
            });
        });
    });

    console.log("Load http routing.");

    // Global Routing
    const GR = new router();

    GR.get('/',function *(next) {
        this.render("projects",{ projects : this.projects });
    });

    // Aplly routing to the middleware!
    app.use( GR.routes() );
    app.use( GR.allowedMethods() );

    // Listen port!
    app.listen(configuration.port);
    console.timeEnd("httpStart");

    console.log("The server is now started on the port "+configuration.port+" !");
    console.timeEnd("startTime");

}).catch( (e) => {
    console.err(e);
});
